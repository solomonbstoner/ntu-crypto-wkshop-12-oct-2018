import string
import sys

key = ""

for i in string.lowercase:
	key += chr((ord(i) - ord('a')) * int(sys.argv[1]) % 26 + ord('a'))

print "Alphabet -> Key"
print "---------------"


for i in range(26):
	print "%s -> %s"%(string.lowercase[i], key[i])
