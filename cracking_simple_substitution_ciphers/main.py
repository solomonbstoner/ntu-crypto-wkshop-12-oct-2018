import ngram_score as ns
import string
import random
import re
from pwn import log
import sys

# This function generates a new random key for the random restart Hill climbing algorithm
def generate_random_key():
	key = ""
	allowed_letters = list(string.uppercase)
	for i in range(26):
		c = random.choice(allowed_letters)
		allowed_letters.remove(c)
		key += c
	return key

# Exchange two letters in the key at random for optimisation
def exchange_two_letters_in_key(key):
	key = list(key)
	index_b = index_a = random.randint(0,25)
	while index_b == index_a:
		index_b = random.randint(0,25)
	temp = key[index_a]
	key[index_a] = key[index_b]
	key[index_b] = temp
	return "".join(str(i) for i in key)


# Decrypt the ciphertext using the key. Returns the plaintext.
def decrypt_ciphertext(ciphertext, key):
	ciphertext = ciphertext.upper()
	plaintext = ""
	for c in ciphertext:
		if not c.isalpha():
			plaintext += c
			continue
		key = list(key)
		#print "%s -> %s"%(c, chr(ord('a') + key.index(c)))
		plaintext += chr(ord('A') + key.index(c))
	return plaintext	


# Test the fitness of the key by decrypting the given ciphertext, then checking the ngram score of the resultant plaintext
def test_fitness(ngram_score, ciphertext, key):
	plaintext = decrypt_ciphertext(ciphertext, key)
	plaintext = plaintext.replace(" ","").replace("\n","")
	plaintext = plaintext.translate(None, string.punctuation)
	return fitness.score(plaintext)


def hillclimb(
	generate_random_key,
	exchange_two_letters_in_key,
	test_fitness,
	max_evaluations,
	ciphertext,
	ngram_score
	):
	'''
	hillclimb until either max_evaluations
	is reached or we are at a local optima
	'''
	best_key=generate_random_key()
	best_fitness=test_fitness(ngram_score, ciphertext, best_key)

	num_evaluations=1

	while num_evaluations < (max_evaluations):
		# examine moves around our current position
		move_made=False
		while True:
			next_key = exchange_two_letters_in_key(best_key)
			#print "Testing next key : %s, Best key : %s"%(next_key, best_key)
			if num_evaluations >= max_evaluations:
				break
		    
			# see if this move is better than the current
			next_fitness=test_fitness(ngram_score, ciphertext, next_key)
			#print "Testing key : %s, Fitness : %f"%(best_key, next_fitness)
			p.status("Testing next key : %s\n Fitness : %s\n Best key : %s\n Best fitness : %d"%(next_key, next_fitness, best_key, best_fitness))
			num_evaluations+=1
			if next_fitness > best_fitness:
				best_key=next_key
				best_fitness=next_fitness
				move_made=True
				break # depth first search
	    
		if not move_made:
			break # we couldn't find a better move 
				# (must be at a local maximum)

	return (num_evaluations,best_fitness,best_key)


def hillclimb_and_restart(
	generate_random_key,
	exchange_two_letters_in_key,
	test_fitness,
	max_evaluations,
	ciphertext,
	ngram_score
	):
	'''
	repeatedly hillclimb until max_evaluations is reached
	'''
	best_key=None
	best_fitness=-10000

	num_evaluations=0
	while num_evaluations < max_evaluations:
		remaining_evaluations=max_evaluations-num_evaluations
		evaluated,found_fitness,found_key=hillclimb(
		generate_random_key,
		exchange_two_letters_in_key,
		test_fitness,
		max_evaluations/10,
		ciphertext,
		ngram_score)
        
		num_evaluations+=evaluated
		print  "Best overall key : %s, Best found key : %s"%(best_key, found_key)
		if found_fitness > best_fitness or best_key is None:
			best_fitness=found_fitness
			best_key=found_key
        
	return (num_evaluations,best_fitness,best_key)


ciphertext = open(sys.argv[1]).read()
fitness = ns.ngram_score('quadgrams.txt')

p = log.progress('')

# Do a total of 20000 tries
num_evaluations, best_fitness, best_key = hillclimb_and_restart(
generate_random_key,
exchange_two_letters_in_key,
test_fitness,
20000,
ciphertext,
fitness)

p.success('Found the key : %s\n'%(best_key))

plaintext = decrypt_ciphertext(ciphertext, best_key)

print plaintext
