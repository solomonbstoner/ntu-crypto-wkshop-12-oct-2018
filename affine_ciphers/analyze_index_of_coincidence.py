# This Python script analyses the index of coincidence
# Requires Python 3

import sys

def index_of_coincidence():
	probability_of_coincidence = 0.0
	for c in cipher_frequency:
		probability_of_coincidence += (cipher_frequency[c] / total_num_of_chars) * (cipher_frequency[c] - 1) / (total_num_of_chars - 1)
		print("Index of coincidence for %s : %d/%d * %d/%d" % (c, cipher_frequency[c], total_num_of_chars, cipher_frequency[c] - 1, total_num_of_chars - 1))
	return probability_of_coincidence


ciphertext = open(sys.argv[1]).read()

cipher_frequency = {}

total_num_of_chars = 0

for c in ciphertext:
	if not c.isalpha():
		continue

	total_num_of_chars += 1
	if c in cipher_frequency:
		cipher_frequency[c] += 1
	else:
		cipher_frequency[c] = 1

print ("Total number of characters : %d" % total_num_of_chars)

print ("\nAnalysis of individual letters:")
print ("-------------------------------")


for c in cipher_frequency:
	percentage_frequency = cipher_frequency[c] / total_num_of_chars * 100.00
	print("%s :	%d	:	%f%%" % (c, cipher_frequency[c], percentage_frequency ))

print ("-------------------------------")

print ("\nAnalysing Index of coincidence of the cipher :")
print ("-------------------------------")

print ("Index of coincidence : %f" % index_of_coincidence())
print ("-------------------------------")
