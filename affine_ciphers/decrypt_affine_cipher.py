import sys

if(len(sys.argv) != 3):
	print("k = sys.argv[1], m = sys.argv[2]")
	exit(1)

# return (g, x, y) a*x + b*y = gcd(x, y)
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = egcd(b % a, a)
        return (g, y - (b // a) * x, x)

# x = mulinv(b) mod n, (x * b) % n == 1
def mulinv(b, n):
    g, x, _ = egcd(b, n)
    if g == 1:
        return x % n

ciphertext = open('ciphertext','r').read()

plaintext = ""

k = int(sys.argv[1])
m = int(sys.argv[2])

ciphertext = ciphertext.replace(" ","").replace("\n","")

print('Ciphertext: \n' + ciphertext)
print('\n')

k_inverse = mulinv(k, 26)

for c in ciphertext:
	print("-----------------")
	print("Cipher: " + c)
	c = c.upper()
	c = ord(c) - ord('A')		# Converts 'A'=0, 'B'=1... 'Z'=25
	p = k_inverse * ( c - m )
	p = p % 26
	print("%d ≡ %d * (%d - %d) (mod 26)" % (p, k_inverse, c, m))
	p = chr(p + ord('A'))		# Converts 0='A', 1='B'... 25='Z'
	print("Plaintext: " + p)
	print("-----------------")
	plaintext += p

print('Plaintext: \n' + plaintext)
exit(0)

