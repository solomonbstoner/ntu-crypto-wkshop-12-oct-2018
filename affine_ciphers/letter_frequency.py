import matplotlib.pyplot as plt
import numpy as np

cipher_file = open('ciphertext','r')
ciphertext = cipher_file.read()
cipher_file.close()

letter_freq_dict = {}
total_num_of_chars = 0

for c in ciphertext:
	if not c.isalpha():
		continue
	total_num_of_chars += 1
	if c in letter_freq_dict:
		letter_freq_dict[c] += 1
	else:
		letter_freq_dict[c] = 1


x_axis = letter_freq_dict.keys()
y_axis = letter_freq_dict.values()
ind = np.arange(len(x_axis))
plt.bar(ind, y_axis)
plt.xticks(ind, x_axis)
plt.show()
